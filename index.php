<?php get_header(); ?>

<?php get_template_part( 'template_parts/slider', 'index' ); ?>

<?php get_template_part( 'template_parts/news', 'index' ); ?>

<?php get_template_part( 'template_parts/video', 'index' ); ?>

<?php get_template_part( 'template_parts/partners', 'index' ); ?>

<?php get_footer(); ?>

