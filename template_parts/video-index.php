
<section class="video">
  <div class="container">
    <h2>видеотека</h2>
    <div class="owl-carousel owl-theme video-library">

    <?php
// параметры по умолчанию
$posts = get_posts( array(
	'numberposts' => 5,
	'category'    => 1,
	'orderby'     => 'date',
	'order'       => 'DESC',
	'include'     => array(),
	'exclude'     => array(),
	'meta_key'    => '',
	'meta_value'  =>'',
	'post_type'   => 'post',
	'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
) );
foreach( $posts as $post ): ?>

<?php setup_postdata($post); ?>

<?php $meta = get_post_meta($post->ID, 'url');?>
        <div class="item-video">
        <a
          class="owl-video"
          href="<?php echo $meta[0]; ?>"
        ></a>
        <h4>
        <?php the_title(); ?>
        </h4>
      </div>

<?php endforeach; wp_reset_postdata();?> 
    
    </div>
  </div>
</section>