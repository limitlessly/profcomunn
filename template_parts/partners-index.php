<section class="partners">
  <div class="container">
    <div class="owl-carousel owl-theme partners-library">
      <div class="item">
        <img src="<?php bloginfo('template_url'); ?>/img/1.png" alt="" />
        <a href="https://eseur.ru/" target="_blank"><h4>Общероссийский Профсоюз образования</h4></a>
      </div>
      <div class="item">
        <img src="<?php bloginfo('template_url'); ?>/img/svg/logotip_UMPiVR-ITOG1.svg" alt=""/>
        <a href="http://www.uvr.unn.ru/" target="_blank"
          ><h4>УМПиВР</h4></a
        >
      </div>
      <div class="item">
        <img src="<?php bloginfo('template_url'); ?>/img/2.png" alt="" />
        <a href="http://sksrf.ru/" target="_blank"
          ><h4>студенческий координационный совет</h4></a
        >
      </div>
    </div>
  </div>
</section>