<section class="news">
  <div class="container">
    <h2>новости профсоюза</h2>
    <div class="row justify-content-center equal">
    <?php 
        $catquery = new WP_Query(array('orderby' => 'date', 'order' => 'DESC','posts_per_page' => 3, 'cat' => 205));
    ?>
    <?php while($catquery->have_posts()) : $catquery->the_post(); ?>
    <?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
    <div class="col-xl-4 col-12">
    <article class="news__preview">
          <div class="news__thumbnail">
            <a href="<?php the_permalink(); ?>" class="news__link"
              ><img src="<?php echo $thumbnail_attributes[0]; ?>" alt=""
            /></a>
          </div>
          <h3 class="news__title">
            <a href="<?php the_permalink(); ?>" class="news__link"
              ><?php the_title(); ?></a
            >
          </h3>
          <div class="news__info">
            <img src="<?php bloginfo('template_url'); ?>/img/svg/clock.svg" alt="" class="news__info__icon" /><span
              ><?php the_time('j.m.Y'); ?></span
            >
            <img src="<?php bloginfo('template_url'); ?>/img/svg/eye.svg" alt="" class="news__info__icon" /><span
              ><?php if(function_exists('the_views')) { the_views(); } ?></span
            >
          </div>
          <p>
          <?php the_excerpt(); ?>
          </p>
        </article>
      </div>    
<?php endwhile; ?> 
    </div>
  </div>
</section>