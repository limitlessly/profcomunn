$(document).ready(function() {
  $(".video-library").owlCarousel({
    items: 1,
    loop: true,
    margin: 30,
    video: true,
    nav: true,
    center: true,
    responsive: {
      480: {
        items: 3
      },
      600: {
        items: 3
      }
    }
  });
});
