$(document).ready(function() {
  $(".slider-top").owlCarousel({
    loop: true,
    margin: 0,
    stagePadding: -10,
    autoHeight: true,
    nav: true,
    responsive: {
      0: {
        items: 1,
        autoHeight: true
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  });
});
